package com.geometry;

public class Triangle {
    private double height;
    private double base;

    public Triangle(double height, double base) {
        this.height = height;
        this.base = base;
    }

    public double getArea() {
        return (this.height*this.base*0.5);
    }

    public String toString() {
        return "height: " + this.height + "\nbase: " + this.base;
    }
}
